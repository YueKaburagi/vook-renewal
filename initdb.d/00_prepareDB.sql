-- Initialize Table and Indices.
CREATE TABLE articles (
       article_ix uuid PRIMARY KEY DEFAULT gen_random_uuid(),
       name text,
       uri text,
       thumbnail_uri text,
       tags text[],
       comment text,
       time_created timestamp with time zone DEFAULT CURRENT_TIMESTAMP(0) NOT NULL,
       time_updated timestamp with time zone DEFAULT CURRENT_TIMESTAMP(0)
);

CREATE INDEX articles_by_tag ON articles USING GIN (tags);
CREATE INDEX articles_by_ix ON articles USING Hash (article_ix);


CREATE TABLE crawler_memo (
       article_ix uuid PRIMARY KEY REFERENCES articles ON DELETE CASCADE,
       last_crawled timestamp with time zone
);

CREATE INDEX crawler_memo_by_ix ON crawler_memo USING Hash (article_ix);
CREATE INDEX crawler_memo_by_last_crawled ON crawler_memo USING BTree (last_crawled ASC NULLS FIRST);
