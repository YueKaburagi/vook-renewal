package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/joho/godotenv"
	"github.com/lib/pq"
)

const ( ISO8601_2004ex_sec_4_3_2 = "2006-01-02T15:04:05.999999999Z" )

func strOr(d string, s string) string {
	if s == "" {
		return d
	}
	return s
}

func dummyEchoHandler(w http.ResponseWriter, r *http.Request) {
	length, err := strconv.Atoi(r.Header.Get("Content-Length"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	body := make([]byte, length)
	_, err = r.Body.Read(body)
	if err != nil && err != io.EOF {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
	_, _ = w.Write(body)
}

// Returns `d` if `ns` is null, otherwise `ns.String`.
//
// Pure function.
func stringOr(d string, ns sql.NullString) string {
	if ns.Valid {
		return ns.String
	} else {
		return d
	}
}

// Generate response for OPTIONS request.
// Returns true if done, otherwise false.
func corsOkOPTIONS(w http.ResponseWriter, r *http.Request) bool {
	if r.Method == "OPTIONS" {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.WriteHeader(http.StatusOK)
		return true
	}
	return false
}

// Pass request has such `method`, or set response status BadRequest.
// Returns true if accepted, otherwise false.
func passMethod(method string, w http.ResponseWriter, r *http.Request) bool {
	if r.Method != method {
		w.WriteHeader(http.StatusBadRequest)
		return false
	}
	return true
}


// Take []byte from request body.
func takeBody(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	requestSize, err := strconv.Atoi(r.Header.Get("Content-Length"))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(err)
		return []byte{}, err
	}
	requestBody := make([]byte, requestSize)
	_, err = r.Body.Read(requestBody)
	if err != nil && err != io.EOF {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return []byte{}, err
	}
	return requestBody, nil
}

func connectDB(w http.ResponseWriter) (*sql.DB, error) {
	dbUser := os.Getenv("POSTGRES_USER")
	dbDB := os.Getenv("POSTGRES_DB")
	dbPass := os.Getenv("POSTGRES_PASSWORD")
	dbHost := strOr("localhost", os.Getenv("DB_HOST"))
	dbPort := strOr("5432", os.Getenv("DB_PORT"))
	if dbUser == "" {
		log.Fatal("Require env: POSTGRES_USER")
	}
	if dbDB == "" {
		log.Fatal("Require env: POSTGRES_DB")
	}
	if dbPass == "" {
		log.Fatal("Require env: POSTGRES_PASSWORD")
	}

	connString := fmt.Sprintf("user=%s dbname=%s password=%s port=%s host=%s sslmode=disable", dbUser, dbDB, dbPass, dbPort, dbHost)
	db, err := sql.Open("postgres", connString)
	if err != nil {
		w.WriteHeader(http.StatusServiceUnavailable)
		log.Println(err)
		return nil, err
	}
	return db, nil
}

// Generate response with given `v`.
// Require: `v` is marshallable into json.
func corsReturnJSON(w http.ResponseWriter, v interface {}) error {
	bytes, err := json.Marshal(v)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return err
	}
	w.Header().Set("Access-Control-Allow-Origin", "*")
	_, err = w.Write(bytes)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return err
	}
	return nil
}

type ArticleCard struct {
	Ix string `json:"ix"`
	Name string `json:"name"`
	Tags []string `json:"tags"`
	Comment string `json:"comment,omitempty"`
	Thumbnail string `json:"thumbnail,omitempty"`
	Uri string `json:"uri,omitempty"`
}

func querySearchCardsByTagHandler(w http.ResponseWriter, r *http.Request) {
	if corsOkOPTIONS(w,r) {
		return
	}
	if ! passMethod("POST", w, r) {
		return
	}

	body, err := takeBody(w,r)
	if err != nil {
		return
	}

	type QuerySearchByTag struct {
		Tags []string `json:"tags"`
	}

	var query QuerySearchByTag
	err = json.Unmarshal(body, &query)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(err)
		return
	}

	db, err := connectDB(w)
	if err != nil {
		return
	}
	defer db.Close()

	// Query文は使う DB に従う
	// 配列型は 通常SQL にないので pq.Array に変換してもらう
	rows, err := db.Query("SELECT article_ix, name, tags, comment, uri, thumbnail_uri FROM articles WHERE articles.tags @> $1", pq.Array(query.Tags))
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}
	defer rows.Close()

	resp := make([]ArticleCard, 0)
	for rows.Next() {
		var cName sql.NullString
		var cComment sql.NullString
		var cUri sql.NullString
		var cThumbnail sql.NullString
		var card ArticleCard
		// 配列受け取りも 通常SQL の範疇外なので pq.Array を経由させる
		err = rows.Scan(&card.Ix, &cName, pq.Array(&card.Tags), &cComment, &cUri, &cThumbnail)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			return
		}
		card.Name = stringOr("", cName)
		card.Comment = stringOr("", cComment)
		card.Uri = stringOr("", cUri)
		card.Thumbnail = stringOr("", cThumbnail)
		resp = append(resp, card)
	}
	
	_ = corsReturnJSON(w, resp)
}

func querySuchURIExists(w http.ResponseWriter, r *http.Request) {
	if corsOkOPTIONS(w,r) {
		return
	}
	if ! passMethod("POST", w, r) {
		return
	}

	body, err := takeBody(w,r)
	if err != nil {
		return
	}

	type QueryURIFind struct {
		Uri string `json:"uri"`
	}
	type RespURIFind struct {
		Name string `json:"name"`
		Uuid string `json:"uuid"`
	}

	var query QueryURIFind
	err = json.Unmarshal(body, &query)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(err)
		return
	}

	db, err := connectDB(w)
	if err != nil {
		return
	}
	defer db.Close()

	rows, err := db.Query("SELECT name, article_ix FROM articles WHERE articles.uri = $1", query.Uri)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}
	defer rows.Close()

	resp := make([]RespURIFind, 0)
	for rows.Next() {
		var cName sql.NullString
		var card RespURIFind
		err = rows.Scan(&cName, &card.Uuid)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			return
		}
		card.Name = stringOr("", cName)
		resp = append(resp, card)
	}

	_ = corsReturnJSON(w, resp)
}

func queryAddNewCard(w http.ResponseWriter, r *http.Request) {
	if corsOkOPTIONS(w,r) {
		return
	}
	if ! passMethod("POST", w, r) {
		return
	}

	body, err := takeBody(w,r)
	if err != nil {
		return
	}

	type QueryAddNewCard struct {
		Name string `json:"name,omitempty"`
		Tags []string `json:"tags"`
		Uri string `json:"uri,omitempty"`
		Comment string `json:"comment,omitempty"`
	}
	
	var query QueryAddNewCard
	err = json.Unmarshal(body, &query)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(err)
		return
	}

	db, err := connectDB(w)
	if err != nil {
		return
	}
	defer db.Close()

	rows, err := db.Query("INSERT INTO articles (name, tags, uri, comment) VALUES ($1, $2, $3, $4) RETURNING article_ix, name, tags, uri, comment", query.Name, pq.Array(query.Tags), query.Uri, query.Comment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}
	defer rows.Close()

	var resp = make([]ArticleCard, 0)
	for rows.Next() {
		var card ArticleCard
		var cName sql.NullString
		var cUri sql.NullString
		var cComment sql.NullString
		err = rows.Scan(&card.Ix, &cName, pq.Array(&card.Tags), &cUri, &cComment)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			return
		}
		card.Name = stringOr("", cName)
		card.Uri = stringOr("", cUri)
		card.Comment = stringOr("", cComment)
		resp = append(resp, card)
	}

	_ = corsReturnJSON(w, resp)
}

func queryEditCard(w http.ResponseWriter, r *http.Request) {
	if corsOkOPTIONS(w, r) {
		return
	}
	if ! passMethod("POST", w, r) {
		return
	}

	body, err := takeBody(w,r)
	if err != nil {
		return
	}

	type QueryEditCard struct {
		Ix string `json:"ix"`
		Name string `json:"name,omitempty"`
		Tags []string `json:"tags"`
		Uri string `json:"uri,omitempty"`
		Comment string `json:"comment,omitempty"`
	}

	var query QueryEditCard
	err = json.Unmarshal(body, &query)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(err)
		return
	}

	db, err := connectDB(w)
	if err != nil {
		return
	}
	defer db.Close()

	rows, err := db.Query("UPDATE articles SET name = $2, tags = $3, uri = $4, comment = $5, time_updated = now() WHERE article_ix = $1 RETURNING article_ix, name, tags, uri, thumbnail_uri, comment", query.Ix, query.Name, pq.Array(query.Tags), query.Uri, query.Comment)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}
	defer rows.Close()

	var resp = make([]ArticleCard, 0)
	for rows.Next() {
		var card ArticleCard
		var cName sql.NullString
		var cURI sql.NullString
		var cThumbnail sql.NullString
		var cComment sql.NullString
		err = rows.Scan(&card.Ix, &cName, pq.Array(&card.Tags), &cURI, &cThumbnail, &cComment)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			return
		}
		card.Name = stringOr("", cName)
		card.Uri = stringOr("", cURI)
		card.Thumbnail = stringOr("", cThumbnail)
		card.Comment = stringOr("", cComment)
		resp = append(resp, card)
	}

	_ = corsReturnJSON(w, resp)

}


func queryTagList(w http.ResponseWriter, r *http.Request) {
	if ! passMethod("GET", w, r) {
		return
	}

	db, err := connectDB(w)
	if err != nil {
		return
	}
	defer db.Close()

	rows, err := db.Query("SELECT q.tag FROM (SELECT DISTINCT unnest(a.tags) AS tag FROM articles AS a) AS q ORDER BY q.tag ASC NULLS LAST")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}
	defer rows.Close()

	type Tag struct {
		Name string `json:"name"`
	}

	var resp = make([]Tag, 0)
	for rows.Next() {
		var tag Tag
		var tName sql.NullString
		err = rows.Scan(&tName)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			return
		}
		if tName.Valid {
			tag.Name = tName.String
			resp = append(resp, tag)
		}
	}

	_ = corsReturnJSON(w, resp)
}

func queryNoThumbnailURIs(w http.ResponseWriter, r *http.Request) {
	if ! passMethod("GET", w, r) {
		return
	}

	db, err := connectDB(w)
	if err != nil {
		return
	}
	defer db.Close()

	rows, err := db.Query(`
SELECT article_ix, uri
FROM articles LEFT OUTER JOIN crawler_memo USING (article_ix)
WHERE thumbnail_uri IS NULL
ORDER BY last_crawled ASC NULLS FIRST
LIMIT 10;
`)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}
	defer rows.Close()

	type MiniCard struct {
		Ix string `json:"ix"`
		Uri string `json:"uri"`
	}

	var resp = make([]MiniCard, 0)
	for rows.Next() {
		var card MiniCard
		err = rows.Scan(&card.Ix, &card.Uri)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			return
		}
		resp = append(resp, card)
	}

	_ = corsReturnJSON(w, resp)
}

func querySetThumbnailURIs(w http.ResponseWriter, r *http.Request) {
	if ! passMethod("POST", w, r) {
		return
	}

	type ThumbnailPack struct {
		Ix string `json:"ix"`
		ThumbnailURIStr string `json:"thumbnail_uri,omitempty"`
		ThumbnailURI sql.NullString `json:"-"`
		LastCrawledStr string `json:"last_crawled,omitempty"`
		LastCrawled sql.NullTime `json:"-"`
	}
	
	body, err := takeBody(w,r)
	if err != nil {
		return
	}

	var query []ThumbnailPack
	err = json.Unmarshal(body, &query)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		log.Println(err)
		return
	}
	for i, q := range query {
		var uri sql.NullString
		if q.ThumbnailURIStr != "" {
			uri.String = q.ThumbnailURIStr
			uri.Valid = true
		} else {
			uri.Valid = false
		}
		query[i].ThumbnailURI = uri

		t, err := time.Parse(ISO8601_2004ex_sec_4_3_2, q.LastCrawledStr)
		var nt sql.NullTime
		if err != nil {
			log.Println(err)
			nt.Valid = false
		} else {
			nt.Valid = true
			nt.Time = t
		}
		query[i].LastCrawled = nt
	}

	db, err := connectDB(w)
	if err != nil {
		return
	}
	defer db.Close()

	stArticle, err := db.Prepare("UPDATE articles SET thumbnail_uri = $2 WHERE article_ix = $1")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}
	defer stArticle.Close()
	stCrawlerMemo, err := db.Prepare(`
INSERT INTO crawler_memo (article_ix, last_crawled)
VALUES ($1, $2)
ON CONFLICT (article_ix)
DO UPDATE SET last_crawled = $2
`)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Println(err)
		return
	}
	defer stCrawlerMemo.Close()


	for _, q := range query {
		_, err := db.Exec("BEGIN")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			return
		}
		_, err = stArticle.Exec(q.Ix, q.ThumbnailURI)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			db.Exec("ROLLBACK")
			continue
		}
		_, err = stCrawlerMemo.Exec(q.Ix, q.LastCrawled)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			db.Exec("ROLLBACK")
			continue
		}
		_, err = db.Exec("END")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			log.Println(err)
			db.Exec("ROLLBACK")
		}
	}

	w.WriteHeader(http.StatusOK)

}


func main() {
	err := godotenv.Load(".env")
	if err != nil {
		log.Println(err)
	}

	http.HandleFunc("/searchbytags", querySearchCardsByTagHandler)
	http.HandleFunc("/urifind", querySuchURIExists)
	http.HandleFunc("/addnewcard", queryAddNewCard)
	http.HandleFunc("/editcard", queryEditCard)
	http.HandleFunc("/taglist", queryTagList)
	http.HandleFunc("/api/v1/nothumbnailuris", queryNoThumbnailURIs)
	http.HandleFunc("/api/v1/setthumbnailuris", querySetThumbnailURIs)
	http.HandleFunc("/echo", dummyEchoHandler)
	fmt.Println("Start up server.")
	port := strOr("8898", os.Getenv("VOOK_GO_PORT"))
	host := strOr("localhost", os.Getenv("VOOK_GO_HOST"))
	addr := fmt.Sprintf("%s:%s", host, port)
	log.Fatal(http.ListenAndServe(addr, nil))
}
