
INSERT INTO articles (name, uri, tags, comment) VALUES
  ('Vue.js', 'https://ja.vuejs.org/', '{"Vue.js", "web", "framework"}', NULL),
  ('Vite', 'https://ja.vitejs.dev/', '{"Vite", "Vue.js", "web", "bundler"}', 'WebpackとかParcelとかそれ系。/vi:t/みたいな発音'),
  ('Stackage', 'https://www.stackage.org/', '{"Haskell", "packages"}', NULL),
  ('Go Packages', 'https://pkg.go.dev/', '{"Go-lang", "packages"}', NULL),
  ('MDN', 'https://developer.mozilla.org/ja/docs/Web', '{"web", "HTML", "CSS", "reference", "JavaScript"}', NULL),
  ('Stylus', 'https://stylus-lang.com/', '{"web", "CSS", "Stylus"}', NULL),
  ('Docker Hub', 'https://hub.docker.com/', '{"docker", "docker images"}', NULL),
  ('Docker Docs', 'https://matsuand.github.io/docs.docker.jp.onthefly/', '{"docker", "reference", "docker-compose"}', NULL);
