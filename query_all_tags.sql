
SELECT q.tag
FROM (
     SELECT DISTINCT unnest(a.tags) AS tag FROM articles AS a
) AS q ORDER BY q.tag ASC NULLS LAST;
