import './assets/main.css'

import { createApp } from 'vue'
import App from './AltApp.vue'

createApp(App).mount('#app')

