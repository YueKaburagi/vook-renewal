module Crawler.Options.Types (Option(..)) where

data Option = DryRun
            | ImmediateRun
            deriving (Eq, Show)
