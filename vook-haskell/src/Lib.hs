{-# LANGUAGE OverloadedStrings, DeriveGeneric, LambdaCase #-}

module Lib
    ( main
    ) where

import Network.Wreq (defaults, post, getWith, header, responseStatus, responseBody, statusCode)
import qualified Network.URI as URI
import Control.Lens (view, (^.), over, Traversal', mapMOf)
import Text.XML.Hexml (parse, inner, childrenBy, Node)
import GHC.Generics
import Data.Aeson (FromJSON(..), ToJSON(..), decode, encode, genericToEncoding, defaultOptions, fieldLabelModifier, camelTo2, genericParseJSON, toJSON)
import qualified Data.Aeson as Aeson
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import System.FilePath (takeFileName, (</>))
import Control.Concurrent (threadDelay)
import System.Directory (createDirectoryIfMissing)
import Data.Time.Clock (UTCTime, getCurrentTime)
import Data.Validity (isValid, Validity(..), Validation(..), ValidationChain(..))
import qualified Text.HTML.DOM as HTML
import qualified Text.XML as XML
import qualified Text.XML.Cursor as C
import qualified Data.XML.Types as XMLT
import qualified Data.Text as T

import qualified Crawler.Options.Types as O

addUserAgent :: [ByteString] -> [ByteString]
addUserAgent [x] = ["Vook/" <> x]
addUserAgent _ = ["Vook/wreq"]

storeDirectory :: FilePath
storeDirectory = "thumbnails"

data ThumbnailURI = ValidThumbnail String
                  | UncheckedThumbnail String
                  | NoThumbnail
                  deriving (Eq, Show)

_validThumbnail :: Traversal' ThumbnailURI String
_validThumbnail f (ValidThumbnail x) = ValidThumbnail <$> f x
_validThumbnail _ NoThumbnail = pure NoThumbnail

instance ToJSON ThumbnailURI where
  toJSON =
    \case
      ValidThumbnail bs -> Aeson.String $ toText bs
      _  -> Aeson.Null
instance Validity ThumbnailURI where
  validate =
    \case
      ValidThumbnail _ -> Validation mempty
      UncheckedThumbnail s -> Validation [Violated $ show s <> " is not validity checked."]
      NoThumbnail -> Validation [Violated "(empty) is not valid thumbnail."]


data ServeTicket = ServeTicket
  { stIx :: Text
  , stURI :: Text
  } deriving (Show, Generic)

instance FromJSON ServeTicket where
  parseJSON = genericParseJSON $ defaultOptions { fieldLabelModifier = nameRule }
    where
      nameRule = camelTo2 '_' . drop 2

type InternalTicket = ReceiveTicket

data ReceiveTicket = ReceiveTicket
  { rtIx :: Text
  , rtThumbnailURI :: ThumbnailURI
  , rtLastCrawled :: UTCTime -- デフォルトの出力は ISO8601:2004(E) sec. 4.3.2 extended
  } deriving (Show, Generic)

instance ToJSON ReceiveTicket where
  toEncoding = genericToEncoding $ defaultOptions { fieldLabelModifier = nameRule }
    where
      nameRule = camelTo2 '_' . drop 2



main :: [O.Option] -> IO ()
main opts = do
  createDirectoryIfMissing True storeDirectory
  apiServerHost <- fromMaybe "localhost" <$> lookupEnv "API_SERVER_HOST"
  apiServerPort <- fromMaybe "8898" <$> lookupEnv "API_SERVER_PORT"
  if isDryRun
    then do
    putStrLn "Crawler works on dry run mode."
    hFlush stdout
    else pure ()
  if not $ elem O.ImmediateRun opts
    then do
    putStrLn "Waiting stabilize..."
    hFlush stdout -- Docker上ではflushを明示しないといつまでも仕事しないみたい
    threadDelay (1 * 60 * 1000 * 1000) -- APIサーバー以上がstableになるのを確認するのは直ぐ書けないので雑に待たせる
    putStrLn "Stabilized."
    else pure ()
  hFlush stdout
  loop apiServerHost apiServerPort
  where
    isDryRun = elem O.DryRun opts
    loop host port = do
      putStrLn "wake up."
      hFlush stdout
      tickets <- fetchTickets host port
      recv <- ticketCounter isDryRun tickets
      receiveTickets host port recv
      putStrLn "sleep."
      hFlush stdout
      threadDelay (30 * 60 * 1000 * 1000) -- Interval 30 mins.
      loop host port

makeThumbnailCache :: ExtracterType -> String -> ExceptT ByteString IO String
makeThumbnailCache etype uri =
  case etype of
    NicoVideo -> do
      let filepath = cacheFilePath uri
      imageData <- ExceptT $ download uri
      liftIO $ writeFileLBS filepath imageData
      pure filepath
      where
        cacheFileName u = "cache-" <> takeFileName u
        cacheFilePath u = storeDirectory </> cacheFileName u
    Favicon -> pure uri
    _ -> pure uri

ticketCounter :: Bool -> [ServeTicket] -> IO [InternalTicket]
ticketCounter isDryRun = foldlM folder mempty
  where
    one :: ServeTicket -> ExceptT ByteString IO ReceiveTicket
    one ticket = do
      let uri = toString $ stURI ticket
      etype <- if isDryRun
            then print (uriToExtracterType uri, uri) >> pure DryRun
            else pure $ uriToExtracterType uri
      r <- fetchThumbnailInfo etype uri
      hFlush stdout
      if isValid r
        then liftIO $ threadDelay (5 * 1000 * 1000)
        else pure ()
      thumbnailCacheURI <- mapMOf _validThumbnail (makeThumbnailCache etype) r
      time <- liftIO $ getCurrentTime
      if isValid thumbnailCacheURI
        then liftIO $ threadDelay (5 * 60 * 1000 * 1000) -- Interval 5 mins.
        else pure ()
      pure $ vendor ticket thumbnailCacheURI time
    vendor :: ServeTicket -> ThumbnailURI -> UTCTime -> ReceiveTicket
    vendor ticket thumbnail lastCrawled =
      ReceiveTicket (stIx ticket) thumbnail lastCrawled
    folder :: [ReceiveTicket] -> ServeTicket -> IO [ReceiveTicket]
    folder rts st = do
      r <- runExceptT $ one st
      case r of
        Left err -> putStrLn (show err <> " [" <> toString (stURI st) <> "]") >> hFlush stdout >> pure rts
        Right rt -> pure $ rt:rts -- Allow unordered list.


normalizeURI :: String -> ThumbnailURI -> Either ByteString ThumbnailURI
normalizeURI base uri =
  case uri of
    ValidThumbnail v -> Right uri
    UncheckedThumbnail p -> ValidThumbnail  <$> normalizeURI0 base p
    NoThumbnail -> Right uri


normalizeURI0 :: String -> String -> Either ByteString String
normalizeURI0 base path
  | URI.isAbsoluteURI path = Right path
  | otherwise = do
      bURI <- maybeToRight ("Invalid URI " <> encodeUtf8 base) $ URI.parseURI base
      pURI <- maybeToRight ("Invalid URI " <> encodeUtf8 path) $ URI.parseRelativeReference path
      Right . show $ pURI `URI.relativeTo` bURI


download :: String -> IO (Either ByteString LBS.ByteString)
download uri = do
  let opts = over (header "User-Agent") addUserAgent defaults
  response <- getWith opts uri
  case response ^. responseStatus . statusCode of
    200 -> pure . Right $ response ^. responseBody
    other -> pure . Left $ "StatusCode: " <> show other

fetchThumbnailInfo :: ExtracterType -> String -> ExceptT ByteString IO ThumbnailURI
fetchThumbnailInfo et uri =
  case et of
    NicoVideo -> do
      let videoID = takeFileName uri
      let infoURI = "https://ext.nicovideo.jp/api/getthumbinfo/" <> videoID
      lbs <- ExceptT $ download infoURI
      path <- ExceptT . pure . extractThumbnailURI et $ toStrict lbs
      ExceptT . pure $ normalizeURI uri path
    Favicon -> do
      lbs <- ExceptT $ download uri
      case extractThumbnailURI et (toStrict lbs) of
        Left err -> do
          print err
          putStrLn $ "Favicon is not found. [" <> uri <> "]"
          pure NoThumbnail
        Right path -> ExceptT . pure $ normalizeURI uri path
    DryRun -> do
      putStrLn "Phase 'fetch thumnail info' skipped. (dry run)"
      ExceptT . pure $ extractThumbnailURI et "dummy bytestring"
    Unknown -> do
      putStrLn $ "Unknown extracter chosen. [" <> uri <> "]"
      ExceptT . pure $ extractThumbnailURI et "dummy bytestring"


fetchTickets :: String -> String -> IO [ServeTicket]
fetchTickets host port = do
  let opts = over (header "User-Agent") addUserAgent defaults
  let addr = "http://" <> host <> ":" <> port <> "/api/v1/nothumbnailuris"
  response <- getWith opts addr
  let tickets = (decode (response ^. responseBody) :: Maybe [ServeTicket])
  case tickets of
    Nothing -> fail "Fail to fetch tickets."
    Just ts -> pure ts

receiveTickets :: String -> String -> [ReceiveTicket] -> IO ()
receiveTickets _ _ [] = do
  putStrLn "Receiving cancelled: no any scuceed tickets."
receiveTickets host port ticket = do
  let body = encode ticket
  let addr = "http://" <> host <> ":" <> port <> "/api/v1/setthumbnailuris"
  _ <- post addr body
  pure ()


uriToExtracterType :: String -> ExtracterType
uriToExtracterType bs | isPrefixOf "https://www.nicovideo.jp/watch/" bs = NicoVideo
                      | isPrefixOf "https://" bs = Favicon
                      | otherwise = Unknown

data ExtracterType = Unknown
                   | DryRun
                   | NicoVideo
                   | Favicon
                   deriving (Eq, Show)

extractThumbnailURI :: ExtracterType -> ByteString -> Either ByteString ThumbnailURI
extractThumbnailURI et body =
  case et of
    NicoVideo ->
      parse body >>= extractFromNicoThumbInfo
    Favicon ->
      extractFromCommonFavicon . XML.toXMLDocument $ HTML.parseLBS (toLazy body)
    DryRun ->
      Right NoThumbnail
    Unknown ->
      Right NoThumbnail


extractFromNicoThumbInfo :: Node -> Either ByteString ThumbnailURI
extractFromNicoThumbInfo ct =
  UncheckedThumbnail . decodeUtf8 . inner <$> (step1 ct >>= step2 >>= step3)
  where    
    stepDown s = maybeToRight ("no <" <> s <> ">") . listToMaybe . flip childrenBy s
    step1 = stepDown "nicovideo_thumb_response"
    step2 = stepDown "thumb"
    step3 = stepDown "thumbnail_url"

extractFromCommonFavicon :: XMLT.Document -> Either ByteString ThumbnailURI
extractFromCommonFavicon doc =
  UncheckedThumbnail . toString <$>
  (stepDown "head" html >>= halfStep "link" >>= withAttrRelHas "rel" "icon" >>= getAttr "href")
  where
    html = XMLT.documentRoot doc
    stepDown = getFirstChildByNameT
    halfStep = getChilrenByNameT
    withAttr :: Text -> Text -> [XMLT.Element] -> Either ByteString XMLT.Element
    withAttr s t = selectWithAttribute s (\x -> x == t)
    withAttrRelHas :: Text -> Text -> [XMLT.Element] -> Either ByteString XMLT.Element
    withAttrRelHas s t = selectWithAttribute s f
      where
        f attr = elem t $ T.words attr
    getAttr = getAttributeT


selectWithAttribute :: Text -> (Text -> Bool) -> [XMLT.Element] -> Either ByteString XMLT.Element
selectWithAttribute s f =
      maybeToRight ("no attr '" <> encodeUtf8 s <> "'") . listToMaybe . filter (hasAttributeBy (toName s) (f . toFlatText))

getAttributeT :: Text -> XMLT.Element -> Either ByteString Text
getAttributeT s = second toFlatText . maybeToRight ("no attr '" <> encodeUtf8 s <> "'") . getAttribute (toName s)

getFirstChildByNameT :: Text -> XMLT.Element -> Either ByteString XMLT.Element
getFirstChildByNameT s = maybeToRight ("no <" <> encodeUtf8 s <> ">") . listToMaybe . listChildrenBy (toName s)

getChilrenByNameT :: Text -> XMLT.Element -> Either ByteString [XMLT.Element]
getChilrenByNameT s = Right . listChildrenBy (toName s)


getAttribute :: XMLT.Name -> XMLT.Element -> Maybe [XMLT.Content]
getAttribute name e = foldl' cond Nothing (XMLT.elementAttributes e)
  where
    cond x@(Just _) _ = x
    cond _ (n, x) | name == n = Just x
                  | otherwise = Nothing

hasAttributeBy :: XMLT.Name -> ([XMLT.Content] -> Bool) -> XMLT.Element -> Bool
hasAttributeBy name f e = any cond (XMLT.elementAttributes e)
  where
    cond (n, x) | name == n = f x
                | otherwise = False

listChildrenBy :: XMLT.Name -> XMLT.Element -> [XMLT.Element]
listChildrenBy name = filter pred . XMLT.elementChildren
  where
    pred a = XMLT.elementName a == name

listHTMLTag :: Text -> XMLT.Element -> [XMLT.Element]
listHTMLTag k = XMLT.isNamed (toName k)

toName :: Text -> XMLT.Name
toName k = XMLT.Name
           { XML.nameLocalName = k
           , XML.nameNamespace = Nothing
           , XML.namePrefix = Nothing
           }

toFlatText :: [XMLT.Content] -> Text
toFlatText = foldr (\a s -> f a <> s) ""
  where
    f (XMLT.ContentText t) = t
    f (XMLT.ContentEntity e) = "&" <> e <> ";"
