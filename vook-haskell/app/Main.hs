module Main (main) where

import qualified Lib
import qualified Crawler.Options.Types as O
import Options.Applicative

data Options = Options
  { dryrunMode :: Bool
  , immediate :: Bool
  }

optParser :: Parser Options
optParser = Options
  <$> switch ( long "dry-run"
             <> help "Crawler does not fetch any images."
             )
  <*> switch ( long "immediate"
             <> help "Crawler works immediately."
             )

main :: IO ()
main = do
  opts <- execParser optBlock
  Lib.main $ unwind opts
  where
    optBlock = info (optParser <**> helper) (
      fullDesc
      <> progDesc "Thumbnail image crawler for vook."
      )
    unwind o =
      [] <> bool [] [O.DryRun] (dryrunMode o) <> bool [] [O.ImmediateRun] (immediate o)
